const express = require('express');
const bodyParser = require('body-parser');
const dbConfig = require('./database.config.js');
const mongoose = require('mongoose');
const logger = require('morgan');
const nunjucks = require('nunjucks')

const articleRoutes = require('./app/routes/article.routes.js');

// create express app
const app = express();

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }))
// parse requests of content-type - application/json
app.use(bodyParser.json())

app.use(logger('dev'));

app.use('/public', express.static('public'));

nunjucks.configure('app/views', {
    autoescape: true,
    express: app
});

app.use(articleRoutes);

// Configuring the database
mongoose.Promise = global.Promise;

// Connecting to the database
const db = mongoose.connect(dbConfig.url + dbConfig.database, {
    useNewUrlParser: true,
    useUnifiedTopology: true
}).then(() => {
    console.log("Successfully connected to the database")
}).catch(err => {
    console.log('Could not connect to the database. Exiting now...', err);
    process.exit();
});

// listen for requests
app.listen(3000, () => {
    console.log("Server is listening on port 3000");
});
