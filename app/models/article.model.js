const mongoose = require('mongoose');

const ArticleSchema = mongoose.Schema({
    content: String,
    title: String,
    author: String
}, {
    timestamps: true
});

const Article = mongoose.model('Article', ArticleSchema);

Article.findByParam = (params) => {
    console.log('params', params)
    return Article.find(params).exec()
};

module.exports = Article;