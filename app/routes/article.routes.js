const { Router } = require('express');
const Article = require('../models/article.model.js');
const router = new Router();

const articles = require('../controllers/article.controller.js');

router.get('/page/:page?', articles.index);
router.get('/', articles.index);
router.get('/search/:name/:value', articles.search);

router.route('/article/new').get(articles.details).post(articles.create);
router.route('/article/:articleId/edit').get(articles.details).post(articles.update);

// Delete the article with articleId
router.get('/article/:articleId/delete', articles.delete);

module.exports = router;