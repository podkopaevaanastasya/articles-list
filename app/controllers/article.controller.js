const Article = require('../models/article.model.js');

exports.index = async (req, res) => {
    let perPage = 10
    let currentPage = Number(req.params.page) || 1
    let pagesQuantity = 0
    let articlesQuantity = await Article.countDocuments()
    if (articlesQuantity > 0) {
        let articles = await Article.find().limit(perPage).skip(perPage * currentPage - perPage);
        pagesQuantity = Math.ceil(articlesQuantity / 10);
        res.render('index.html', {articles, pagesQuantity, currentPage})
    } else {
        res.render('index.html', {pagesQuantity, message: 'The article list is empty...'})
    }
}

exports.details = async (req, res) => {
    let result = {}
    result = await Article.findById(req.params.articleId)
    res.render('createEditArticle.html', { article: result })
}

// Create and Save a new Article
exports.create = (req, res) => {
    if(!req.body.content || !req.body.title || !req.body.author) {
        return res.status(400).send({
            message: "Article content, title and author can not be empty"
        });
    }

    const article = new Article({
        content: req.body.content,
        title: req.body.title,
        author: req.body.author
    });

    article.save()
        .catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while creating the article."
        });
    })
    res.redirect('/');
};

// Update an Article identified by the articleId in the request
exports.update = (req, res) => {
    console.log('bode', req.body)
    console.log('req.params.articleId', req.params.articleId)
    if(!req.body.content || !req.body.title || !req.body.author) {
        return res.status(400).send({
            message: "Article content, title and author can not be empty"
        });
    }

    Article.findByIdAndUpdate(req.params.articleId, {
        content: req.body.content,
        title: req.body.title,
        author: req.body.author
    }, {new: true})
        .then(article => {
            if(!article) {
                return res.status(404).send({
                    message: "Article not found with id " + req.params.articleId
                });
            }
        }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Article not found with id " + req.params.articleId
            });
        }
        return res.status(500).send({
            message: "Error updating article with id " + req.params.articleId
        });
    });
    res.redirect('/');
};

// Delete an Article with the specified articleId in the request
exports.delete = (req, res) => {
    Article.findByIdAndDelete(req.params.articleId)
        .then(article => {
            if(!article) {
                return res.status(404).send({
                    message: "Article not found with id " + req.params.articleId
                });
            }
        }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "Article not found with id " + req.params.articleId
            });
        }
        return res.status(500).send({
            message: "Could not delete article with id " + req.params.articleId
        });
    });
    res.redirect('/');
};

exports.search = async (req, res) => {
    const params = {}
    let pagesQuantity = 0
    let currentPage = Number(req.params.page) || 1

    params[req.params.name] = { $regex: req.params.value }
    const articles = await Article.find(params)
    if (articles.length > 0) {
        pagesQuantity = Math.ceil(articles.length / 10);
        console.log('pagesQuantity', pagesQuantity)
        res.render('index.html', {articles, pagesQuantity, currentPage})
    } else {
        res.render('index.html', {pagesQuantity, message: 'No search results...'})
    }

}